// Importar a dependência do SQLite3
const sqlite3 = require("sqlite3").verbose()

// Criar o objeto que irá fazer operações no banco de dados
const db = new sqlite3.Database("./src/database/database.db")

module.exports = db

// Utilizar o objeto de banco de dados para nossas operações
db.serialize(() => {
    // resetDb()
    // insertData()
    // deleteData([1])
    // deleteAll()
    // viewData()
})

function resetDb() {
    deleteTable()
    createTable()
}

function createTable() {
    console.log("create table")
    db.run(`
        CREATE TABLE IF NOT EXISTS places (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            image TEXT,
            address TEXT,
            address2 TEXT,
            state TEXT,
            city TEXT,
            items TEXT
        );
    `, function (err) {
        if (err) {
            return console.log(err)
        }
        console.log("Tabela criada com sucesso")
    })
}

function deleteTable() {
    console.log("delete table")
    db.run(`DROP TABLE IF EXISTS places`, function (err) {
        if (err) {
            return console.log(err)
        }
        console.log("Tabela deletada com sucesso")
    })
}

function viewData() {
    console.log("view data")
    db.all(`SELECT * FROM places`, function (err, rows) {
        if (err) {
            return console.log(err)
        }
        console.log("Aqui estão seus registros:")
        console.log(rows)
    })
}

function insertData() {
    console.log("insert data")
    const query = `
        INSERT INTO places (
            name,
            image,
            address,
            address2,
            state,
            city,
            items
        ) VALUES (?, ?, ?, ?, ?, ?, ?);
    `
    const colectoria = [
        "Colectoria",
        "https://images.unsplash.com/photo-1503596476-1c12a8ba09a9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=967&q=80",
        "Guilherme Gemballa, Jardim América",
        "Número 260",
        "Santa Catarina",
        "Rio do Sul",
        "Resíduos Eletrônicos, Lâmpadas"
    ]
    const papersider = [
        "Papersider",
        "https://images.unsplash.com/photo-1528323273322-d81458248d40?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1101&q=80",
        "Guilherme Gemballa, Jardim América",
        "Número 260",
        "Santa Catarina",
        "Rio do Sul",
        "Papéis e Papelão"
    ]
    function afterInsertData(err) {
        if (err) {
            return console.log(err)
        }
        console.log("Cadastrado com sucesso")
        console.log(this)
    }
    db.run(query, colectoria, afterInsertData)
    db.run(query, papersider, afterInsertData)
}

function deleteData(data) {
    console.log("delete data")
    db.run(`DELETE FROM places WHERE id = ?`, data, function (err) {
        if (err) {
            return console.log(err)
        }
        console.log("Registro deletado com sucesso")
    })
}

function deleteAll() {
    console.log("delete all data")
    db.run(`DELETE FROM places`, function (err) {
        if (err) {
            return console.log(err)
        }
        console.log("Registros deletados com sucesso")
    })
}